<nav class="navbar is-margin-l-3 {{ $landing ? 'is-white': 'is-dark' }}">
	<div class="navbar-brand">
		<div class="landing-logo">
			<a class="navbar-item no-hover {{ !$landing ? 'has-text-white' : '' }}" href="/">
				Defc
					<span class="fa fa-css3 has-text-primary"></span>
				stS
			</a>
		</div>
		<a class="navbar-item is-hidden-desktop" href="" target="_blank">
			<span class="icon">
				<i class="fa fa-lg fa-facebook"></i>
			</span>
		</a>
		<a class="navbar-item is-hidden-desktop" href="" target="_blank">
			<span class="icon">
				<i class="fa fa-lg fa-twitter"></i>
			</span>
		</a>
		<a class="navbar-item is-hidden-desktop" href="" target="_blank">
			<span class="icon">
				<i class="fa fa-lg fa-github"></i>
			</span>
		</a>
		<div class="navbar-burger burger">
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
    <div class="navbar-menu">
		<div class="navbar-start is-size-5">
			<a href="" class="navbar-item no-hover is-size-6-mobile">
				Discuss
			</a>
			<a href="" class="navbar-item no-hover is-size-6-mobile">
				Casts
			</a>
			<app-search></app-search>
			@if (!Auth::check())
				<app-my-profile landing="{{ $landing }}"></app-my-profile>
			@else
				<a href="" class="navbar-item no-hover is-size-6-mobile">
					Login
				</a>
				<a href="" class="navbar-item no-hover is-size-6-mobile">
					Sign up
				</a>
			@endif
		</div>
	</div>
</nav>