import SearchBox from './components/navbar/SearchBox.vue';
import MyProfile from './components/navbar/MyProfile.vue';

export const Components = {
	appSearch: SearchBox,
	appMyProfile: MyProfile
}